import sys
import copy
from random import expovariate, gauss, uniform
from user import *
from simulator import *


def printHelp():
    print("usage ./single_traces <file_with_traces>")
    quit()


if(len(sys.argv) == 2):
    filename = sys.argv[1]
    print("Using file ",  filename)
else:
    printHelp()


trees = readFile(filename)
quit()

print("Got tree from traces:", trees)

for tree in trees:
    n_nodes = len(trees[tree].nodes)
    DG = trees[tree]
    #Generate random users with random numbers of base prestige
    users_orig = generateUsersPrestigeFromGraph(100, DG)

    #Copy the original users for simple and progressive mining
    users_simple = copy.deepcopy(users_orig)
    users_prog = copy.deepcopy(users_orig)

    #perform simulation for simple mining
    calculatePrestigeSimple(users_simple, DG)
    generateStats(users_orig, users_simple, DG, 'simple', tree)
    #perform simulation for progressive mining
    calculatePrestige(users_prog, DG)
    generateStats(users_orig, users_prog, DG, 'prog', tree)


printStats()
quit()
