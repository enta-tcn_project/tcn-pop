import sys
from random import expovariate, gauss, uniform
from user import *
from simulator import *
import matplotlib
import random
import copy


font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 12}

matplotlib.rc('font', **font)

stats = {}
stats['size'] = list()
stats['iter'] = list()
stats['paths'] = list()
stats['type'] = list()

for size in range(1, 1000):
    for i in range(1,10):
        G = generateRandomTree(size)

        stats['size'].append(size)
        stats['iter'].append(i)
        stats['paths'].append(len(G.edges))
        stats['type'].append('simple')

        chaini = chain.from_iterable
        roots = (v for v, d in G.in_degree() if d == 0)
        leaves = (v for v, d in G.out_degree() if d == 0)
        all_paths = partial(nx.all_simple_paths, G)
        found_paths = list(chaini(starmap(all_paths, product(roots, leaves))))
        stats['size'].append(size)
        stats['iter'].append(i)
        stats['paths'].append(len(found_paths))
        stats['type'].append('prog')



print(stats)

df = pd.DataFrame(stats)
fig, ax = plt.subplots()
for key, group in df.groupby('type'):
    avg = group.groupby('size')['paths'].mean()
    std = group.groupby('size')['paths'].std()
    ax.set_xlabel("ax label")
    #if(group.get_level_values('type') == 'simple'):
    bx = avg.plot(x='size', y='paths', yerr=std, ax=ax, legend=True);
    #else:
    #    bx = avg.plot(x='d', y='prestige', yerr=std, ax=ax, legend=True, linestyle='dashed');
    bx.set_xlabel("# of Participating Users")
    bx.set_ylabel("# of Submitted Signatures")

plt.legend(labels=['Simple', 'Progressive'])
plt.savefig('overhead.pdf')
