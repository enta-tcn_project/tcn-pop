import sys
from random import expovariate, gauss, uniform
from user import *
from simulator import *
import matplotlib

font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 12}

matplotlib.rc('font', **font)


def printHelp():
    print("usage ./function <number_of_iterations>")
    quit()


if(len(sys.argv) == 2):
    nIterations = int(sys.argv[1])
    print("Running ",  nIterations, " iterations")
else:
    printHelp()
    quit()

users = {}
users[0] = User(0, 80, 0, d=0.05)
users[1] = User(1, 80, 0, d=0.15)
users[3] = User(0, 50, 0, d=0.05)
users[4] = User(1, 50, 0, d=0.15)


# nIterations = number of blocks to run
generateUserStats(users)
for i in range(0, nIterations):
    for user in users.values():
        print("User:", user)
        if(i == 100):
            user.prestige = user.prestige + 200
        if(i == 150):
            user.prestige = user.prestige - 200
        user.update()

    generateUserStats(users)
    print(users_prestige) # in simulator.py file, a list of all user prestige


printUsersStats(users)
