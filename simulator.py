import networkx as nx
import matplotlib.pyplot as plt
from matplotlib import lines
import sys
from itertools import chain
from itertools import product
from itertools import starmap
from functools import partial
from random import expovariate, gauss, uniform
from itertools import groupby
from user_mal import *
import pandas as pd
import matplotlib
import misc

###############################
# Structures to gather stats  #
###############################
stats = {}
stats['node'] = list()
stats['d'] = list()
stats['f'] = list()
stats['sum'] = list()
stats['frequency'] = list()


#############################
# Font settings for graphs  #
#############################
font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 12}
matplotlib.rc('font', **font)


traversed = set()
traversed_mal = set()
traversed_temp = set()

###################################################
# calculate the amount of prestige sent upstream  #
###################################################
def calculateTransfer(node, node_strength, path_strength, prestige_transit, f, b):
    #fee paid by the node
    #The node is added to traversed to pay only once
    if(node not in traversed):
        node_loses = f
        traversed.add(node)
    else:
        node_loses = 0

    # The retain function, user retains some value from downstream and the rest is sent upstream
    node_gains = (prestige_transit * node_strength) / ((path_strength * b) + node_strength)
    #print("Initial transit: ", prestige_transit)
    #print("Node ", node, " looses: ", node_loses, " Node gains: ", node_gains)
    transfered_up = (prestige_transit + node_loses - node_gains)
    #print("Transfered up: ", transfered_up)
    return {'lost': node_loses,
            'gained': node_gains,
            'transfer': transfered_up}


###################################################
# calculate the amount of Maliciousness sent upstream  #
###################################################
def calculateMalTransfer(node, node_strength, path_strength, maliciousness_transit, f, b):
    #fee paid by the node
    #The node is added to traversed to pay only once
    if(node not in traversed_mal):
        #node_loses = f
        node_loses = f
	#no fee paid to rate a transaction
        traversed_mal.add(node)
    else:
        node_loses = 0

    # The retain function, user retains some value from downstream and the rest is sent upstream
    node_gains = (maliciousness_transit * node_strength) / ((path_strength * b) + node_strength)
    #print("Initial transit: ", prestige_transit)
    #print("Node ", node, " looses: ", node_loses, " Node gains: ", node_gains)
    transfered_up = (maliciousness_transit + node_loses - node_gains)
    #print("Transfered up: ", transfered_up)
    return {'lost_mal': node_loses,
            'gained_mal': node_gains,
            'transfer_mal': transfered_up}


################################################
# calculate path strength using max function   #
################################################
def calculatePathStrengthMax(users, path):
    max_prestige = 0
    for node in path:
        if(users[node].prestige > max_prestige):
            max_prestige = users[node].prestige

    return max_prestige

################################################
# calculate path strength using sum function   #
################################################
def calculatePathStrengthSum(users, path):
    sum_prestige = 0
    for node in path:
        sum_prestige += users[node].prestige

    return sum_prestige

#############################
# calculate node strength   #
#############################
def calculateNodeStrength(users, node):
    return users[node].prestige * 2


# [AS] Maliciousness functions

################################################
# calculate mal-path strength using max function   #
################################################
def calculateMalPathStrengthMax(users, path):
    max_maliciousness = 0
    for node in path:
        if(users[node].maliciousness > max_maliciousness):
            max_maliciousness = users[node].maliciousness

    return max_maliciousness

################################################
# calculate mal-path strength using sum function   #
################################################
def calculateMalPathStrengthSum(users, path):
    sum_maliciousness = 0
    for node in path:
        sum_maliciousness += users[node].maliciousness

    return sum_maliciousness

#############################
# calculate mal-node strength   #
#############################
def calculateMalNodeStrength(users, node):
    return users[node].maliciousness * 2


#################################
# Prestige SimpleMining implementation   #
#################################
def calculatePrestigeSimple(users, G, f=20):
    traversed.clear()
    #get roots and leaves to find all the paths in the tree we have to traverse
    chaini = chain.from_iterable
    roots = (v for v, d in G.in_degree() if d == 0) # a node is a root if its in_degree is 0
    leaves = (v for v, d in G.out_degree() if d == 0) # a node is a leave if its out_degree is 0
    all_paths = partial(nx.all_simple_paths, G)
    found_paths = list(chaini(starmap(all_paths, product(roots, leaves)))) # all valid paths in graph G leading from 'root' to 'leave'

    #Traverse the paths we've found
    for path in found_paths:
        if debugging: print("Found path: ", path)
        prestige_sum = 0
        node = path[-1] #the end beneficiary of the path
        initial = node


        if debugging: print("Node: ", node)
        pred = list(G.predecessors(node)) #the contributor of the leaf
        if debugging: print("Predescessor: ", pred)
        prestige_transit = 0
        #go node by node along the path and calculate prestige transfers
        while(pred):
            #if we've more than 1 predescessor, the tree is incorrect
            # [AS] There would only be two or more predecessor nodes only if our path included multiple transfers, and it shouldn't # ( e.g. 0->2, 1->2 )
            if(len(pred) > 1):
                print("More than 1 predescessor")
                quit()
            #move prestige around
            if(node not in traversed):
                 users[node].prestige -= f
                 users[pred[0]].prestige += f #pred[0] is int, an index to the user number
                 if debugging: print("Moving ", f, " prestige from", node, "to", pred[0])
                 # print("Moving ", f, " prestige from", node, "to", pred[0])
                 traversed.add(node)
                 final = pred[0] # [AS] Node receiving the prestige

            node = pred[0]
            # move up the path
            pred = list(G.predecessors(node))

        # [AS] note: Keep track of ultimate prestige transfer
        if debugging: print("Overall moved ", f, " prestige from", initial, "to", final, "!\n")

        #[AS] note: Keep track of total transfers (used in maliciousness calculation)
        users[final].addTransfer()
        #print("Overall moved ", f, " prestige from", initial, "to", final, "!\n")

    return;


#################################
# [AS] Checks whether the original sender is trusted! If not, the entire transfer is negated #
#################################

def calculatePrestigeSimpleV2(users, G, f=20):
    traversed.clear()
    #get roots and leaves to find all the paths in the tree we have to traverse
    chaini = chain.from_iterable
    roots = (v for v, d in G.in_degree() if d == 0) # a node is a root if its in_degree is 0
    leaves = (v for v, d in G.out_degree() if d == 0) # a node is a leave if its out_degree is 0
    all_paths = partial(nx.all_simple_paths, G)
    found_paths = list(chaini(starmap(all_paths, product(roots, leaves)))) # all valid paths in graph G leading from 'root' to 'leave'

    #Traverse the paths we've found
    for path in found_paths:
        print("[SimpleMining] Found path: ", path)
        prestige_sum = 0
        node = path[-1] #the end beneficiary of the path
        initial = node
        print("[SimpleMining] Original prestige for node ", users[initial].id , " is: ", users[initial].prestige)

        if debugging: print("Node: ", node)
        pred = list(G.predecessors(node)) #the contributor of the leaf

        if debugging: print("Predescessor: ", pred)
        prestige_transit = 0

        #go node by node along the path and calculate prestige transfers
        while(pred):
            #if we've more than 1 predescessor, the tree is incorrect
            # [AS] There would only be two or more predecessor nodes only if our path included multiple transfers, and it shouldn't # ( e.g. 0->2, 1->2 )
            if(len(pred) > 1):
                print("More than 1 predescessor")
                quit()
            #move prestige around
            if(node not in traversed):
                 print("[SimpleMining] Original prestige for node ", users[pred[0]].id , " is: ", users[pred[0]].prestige)
                 users[node].prestige -= f
                 users[pred[0]].prestige += f #pred[0] is int, an index to the user number
                 #if debugging: print("Moving ", f, " prestige from", node, "to", pred[0])
                 print("Moving ", f, " prestige from", node, "to", pred[0])
                 print("[SimpleMining] New prestige for node ", users[pred[0]].id , " is: ", users[pred[0]].prestige)
                 traversed.add(node)
                 final = pred[0] # [AS] Node receiving the prestige

            node = pred[0]
            # move up the path
            pred = list(G.predecessors(node))

        # [AS] note: Keep track of ultimate prestige transfer
        #if debugging: print("Overall moved ", f, " prestige from", initial, "to", final, "!\n")
        print("[SimpleMining] Overall moved ", f, " prestige from", initial, "to", final, "!")

        # [AS] Added behavior based on contributor maliciousness
        # Beneficiaries are now trying to avoid receiving content from malicious users
       # print("Contributor maliciousness is: ", users[final].maliciousness)
        if (users[final].isTrusted()):
            # We add the current transfer to the total sum
            users[final].addTransfer()
        else:
            # The transfer is negated; the content should not be received
            print("[SimpleMining] User ", users[final].id, " was not trusted; transfer is negated")
            users[initial].prestige += f
            users[final].prestige -=f

        print("[SimpleMining] Final prestige for node ", users[initial].id , " is: ", users[initial].prestige)
        for n in path:
            print ("[SimpleMining] Final prestige for node ", users[n].id , " is: ", users[n].prestige, "\n")

    return;


def calculatePrestigeSimpleV3(users, G, f=20):
    traversed.clear()
    traversed_temp.clear()
    #get roots and leaves to find all the paths in the tree we have to traverse
    chaini = chain.from_iterable
    roots = (v for v, d in G.in_degree() if d == 0) # a node is a root if its in_degree is 0
    leaves = (v for v, d in G.out_degree() if d == 0) # a node is a leave if its out_degree is 0
    all_paths = partial(nx.all_simple_paths, G)
    found_paths = list(chaini(starmap(all_paths, product(roots, leaves)))) # all valid paths in graph G leading from 'root' to 'leave'

    #Traverse the paths we've found
    for path in found_paths:
        print("[SimpleMining] Found path: ", path)
        prestige_sum = 0
        node = path[-1] #the end beneficiary of the path
        initial = node
        if debugging: print("Node: ", node)
        pred = list(G.predecessors(node)) #the contributor of the leaf
        if debugging: print("Predescessor: ", pred)
        prestige_transit = 0

        for n in path:
            print("[SimpleMining] Original prestige for node ", users[n].id , " is: ", users[n].prestige)

        #go node by node along the path and calculate prestige transfers
        while(pred):
            #if we've more than 1 predescessor, the tree is incorrect
            # [AS] There would only be two or more predecessor nodes only if our path included multiple transfers, and it shouldn't # ( e.g. 0->2, 1->2 )
            if(len(pred) > 1):
                print("More than 1 predescessor")
                quit()

            #find the node that is going to receive the prestige, if trusted
            if(node not in traversed):
                 traversed_temp.add(node)
                 final = pred[0] # [AS] Node receiving the prestige

            node = pred[0]
            # move up the path
            pred = list(G.predecessors(node))

        if (users[final].isTrusted()):
            # [AS] Add the current transfer to the node's sum of total tranfers, move prestige around, then add the temporarily stored nodes to the set of all 'traversed' nodes
            users[final].addTransfer()
            users[initial].prestige -= f
            users[final].prestige += f
            print("[SimpleMining] Overall moved ", f, " prestige from", initial, "to", final, "!")
            for t in traversed_temp:
                traversed.add(t)
        else:
            # The transfer is negated; the content should not be received
            print("[SimpleMining] User ", users[final].id, " was not trusted; transfer is negated")

        #print("[SimpleMining] Final prestige for node ", users[initial].id , " is: ", users[initial].prestige)
        for n in path:
            print ("[SimpleMining] Final prestige for node ", users[n].id , " is: ", users[n].prestige)
        print("")
    return;


#################################
# Maliciousness - SimpleMining  #
#################################

#currently an unmodified copy of prestige/simplemining
def calculateMaliciousnessSimple(users, G, f=20):
    traversed_mal.clear()
    #get roots and leaves to find all the paths in the tree we have to traverse
    chaini = chain.from_iterable
    roots = (v for v, d in G.in_degree() if d == 0)
    leaves = (v for v, d in G.out_degree() if d == 0)
    all_paths = partial(nx.all_simple_paths, G)
    found_paths = list(chaini(starmap(all_paths, product(roots, leaves))))

    #Traverse the paths we've found
    for path in found_paths:
        #print("Found path: ", path)
        maliciousness_sum = 0
        node = path[-1] #the end beneficiary of the path
        initial = node # [AS] Node transferring its prestige
        #print("Node: ", node)
        pred = list(G.predecessors(node)) #the contributor of the leaf
        #print("Predescessor: ", pred)
        maliciousness_transit = 0
        #go node by node along the path and calculate prestige transfers
        while(pred):
            #if we've more than 1 predescessor, the tree is incorrect
            if(len(pred) > 1):
                print("More than 1 predescessor")
                quit()
            #move prestige around
            if(node not in traversed_mal):
                 users[node].maliciousness -= f
                 users[pred[0]].maliciousness += f #pred[0] is int, an index to the user number
                 if debugging: print("Moving ", f, " maliciousness from", node, "to", pred[0])
                 # print("Moving ", f, " maliciousness from", node, "to", pred[0])
                 traversed_mal.add(node)
                 final = pred[0] # [AS] Node receiving the maliciousness

            node = pred[0]
            # move up the path
            pred = list(G.predecessors(node))

            # [AS] note: Keep track of ultimate maliciousness transfer
        if debugging: print("Overall moved ", f, " maliciousness from", initial, "to", final, "!\n")
        #print("Overall moved ", f, " maliciousness from", initial, "to", final, "!\n")

    return;

# based on calculateMaliciousnessSimple()
def calculateMaliciousnessV2(user):
    print("Printing user! ", user)

    if (user.transfers):
        user.maliciousness = (user.fake / user.transfers)
    else:
        user.maliciousness = 0
    #print("User malice is equal to: ", user.maliciousness)
    
    return;

####################################
# Prestige ProgressiveMining implementation #
####################################
def calculatePrestige(users, G, f=20, b=0.5):
    traversed.clear()
    #get roots and leaves to find all the paths in the tree we have to traverse
    chaini = chain.from_iterable
    roots = (v for v, d in G.in_degree() if d == 0)
    leaves = (v for v, d in G.out_degree() if d == 0)
    all_paths = partial(nx.all_simple_paths, G)
    found_paths = list(chaini(starmap(all_paths, product(roots, leaves))))

    #Traverse the paths we've found
    for path in found_paths:
        #print("Found path: ", path)
        prestige_sum = 0
        node = path[-1]
        #print("Node: ", node)
        #pred = list(G.predecessors(node))
        pred = path[-2]
        #print("Predescessor: ", pred)
        prestige_transit = 0
        #go node by node along the path and calculate prestige transfers
        while(True):
            #if we've more than 1 predescessor, the tree is incorrect
            #now it doesn't apply, as we have a more efficient way of assigning pred and skip the check
            #if(len(pred) > 1):
            #    print("More than 1 predescessor")
            #    quit()
            #calculate path strength, not the most efficient, but flexible and easy to write
            #calculating nx.shortest_path all the time can be a bottleneck
            path_before = path[:-1]#nx.shortest_path(G, 0, pred)
            #print("Path predescessing: ", path_before)
            path_strength = calculatePathStrengthMax(users, path_before)
            node_strength = calculateNodeStrength(users, node)

            #move prestige around
            #print("Node ", node, " Node's strength: ", node_strength, "Path strength: ", path_strength)
            info = calculateTransfer(node, node_strength, path_strength, prestige_transit, f=f, b=b)
            prestige_transit = info['transfer']
            users[node].gained += info['gained'] - info['lost']
            path = path[:-1]
            node = path[-1]
            #print("New node: ", node)
            #print("Path: ", path, " len: ", len(path), " is lower than 1?", len(path) < 1)
            if(len(path) < 2):
                break
            pred = path[-2]
            #print("New pred: ", pred)
        #add prestige to the root
        #print("Adding ", prestige_transit, "to root ", node)
        users[node].gained += prestige_transit

    for user in users:
        users[user].prestige += users[user].gained
        users[user].gained = 0

    return;


####################################
# Prestige ProgressiveMining implementation #
####################################
def calculatePrestigeV2(users, G, f=20, b=0.5):
    traversed.clear()
    #get roots and leaves to find all the paths in the tree we have to traverse
    chaini = chain.from_iterable
    roots = (v for v, d in G.in_degree() if d == 0)
    leaves = (v for v, d in G.out_degree() if d == 0)
    all_paths = partial(nx.all_simple_paths, G)
    found_paths = list(chaini(starmap(all_paths, product(roots, leaves))))

    #Traverse the paths we've found
    for path in found_paths:
        print("[ProgressiveMining] Found path: ", path)
        node = path[-1]
        print("Original end beneficiary: ", node)

        # [AS] Added new behaviour, according to node maliciousness values
        new_path = []
        path_length = len(path)
        print("Path length is ", path_length)
        for usr in path:
            print("Node ", usr, ", Maliciousness: ", users[usr].maliciousness)
            if (users[usr].isTrusted()):
                print("Adding user", usr, " to valid path!")
                new_path.append(usr)
            else:
                print("User", usr, " was not deemed trustworthy. The content object was not forwarded any further down the path")
                new_path.append(usr)
                break
        print("New path is ", new_path)
        path = new_path
        if len(path)<2:
            print("[ProgressiveMining] Path is not valid; abort")
            break

        # [AS] End of new behavior

        # if (users[final].isTrusted()):
        #     # We add the current transfer to the total sum
        #     users[final].addTransfer()
        # else:
        #     # The transfer is negated; the content should not be received
        #     users[initial].prestige += f
        #     users[final].prestige -=f

        prestige_sum = 0
        node = path[-1]
        print("New end beneficiary: ", node)
        #pred = list(G.predecessors(node))
        pred = path[-2]
        #print("Predescessor: ", pred)
        prestige_transit = 0
        #go node by node along the path and calculate prestige transfers
        while(True):
            #if we've more than 1 predescessor, the tree is incorrect
            #now it doesn't apply, as we have a more efficient way of assigning pred and skip the check
            #if(len(pred) > 1):
            #    print("More than 1 predescessor")
            #    quit()
            #calculate path strength, not the most efficient, but flexible and easy to write
            #calculating nx.shortest_path all the time can be a bottleneck
            path_before = path[:-1] #nx.shortest_path(G, 0, pred)
            #print("Preceding path: ", path_before)
            path_strength = calculatePathStrengthMax(users, path_before)
            node_strength = calculateNodeStrength(users, node)

            #move prestige around
            #print("Node ", node, " Node's strength: ", node_strength, "Path strength: ", path_strength)
            info = calculateTransfer(node, node_strength, path_strength, prestige_transit, f=f, b=b)
            prestige_transit = info['transfer']
            users[node].gained += info['gained'] - info['lost']

            path = path[:-1]
            node = path[-1]

            #[AS] Used for maliciousness calculation
            users[node].addTransfer()
            print("Added a valid transfer to user ", node)
            
            #print("New node: ", node)
            #print("Path: ", path, " len: ", len(path), " is lower than 1?", len(path) < 1)
            if(len(path) < 2):
                break
            pred = path[-2]
            #print("New pred: ", pred)
        #add prestige to the root
        print("Adding ", prestige_transit, "to root ", node)
        users[node].gained += prestige_transit

    for user in users:
        users[user].prestige += users[user].gained
        users[user].gained = 0

    return;



####################################
# Maliciousness   -   ProgMining   #
####################################

#Currently an unmodified copy of prestige/progressive mining 

def calculateMaliciousness(users, G, f=20, b=0.5):
    traversed_mal.clear()
    #get roots and leaves to find all the paths in the tree we have to traverse
    chaini = chain.from_iterable
    roots = (v for v, d in G.in_degree() if d == 0)
    leaves = (v for v, d in G.out_degree() if d == 0)
    all_paths = partial(nx.all_simple_paths, G)
    found_paths = list(chaini(starmap(all_paths, product(roots, leaves))))

    #Traverse the paths we've found
    for path in found_paths:
        if debugging: print("Found path: ", path)
        maliciousness_sum = 0
        node = path[-1]
        #print("Node: ", node)
        #pred = list(G.predecessors(node))
        pred = path[-2]
        #print("Predescessor: ", pred)
        maliciousness_transit = 0
        #go node by node along the path and calculate prestige transfers
        while(True):
            #if we've more than 1 predescessor, the tree is incorrect
            #now it doesn't apply, as we have a more efficient way of assigning pred and skip the check
            #if(len(pred) > 1):
            #    print("More than 1 predescessor")
            #    quit()
            #calculate path strength, not the most efficient, but flexible and easy to write
            #calculating nx.shortest_path all the time can be a bottleneck
            path_before = path[:-1]#nx.shortest_path(G, 0, pred)
            if debugging: print("Path before: ", path_before)
            #print("Path predescessing: ", path_before)
            path_mal_strength = calculateMalPathStrengthMax(users, path_before)
            node_mal_strength = calculateMalNodeStrength(users, node)

            #move prestige around
            #print("Node ", node, " Node's strength: ", node_strength, "Path strength: ", path_strength)
            info = calculateMalTransfer(node, node_mal_strength, path_mal_strength, maliciousness_transit, f=f, b=b)
            maliciousness_transit = info['transfer_mal']
            users[node].gained_mal += info['gained_mal'] - info['lost_mal']
            path = path[:-1]
            node = path[-1]
            #print("New node: ", node)
            #print("Path: ", path, " len: ", len(path), " is lower than 1?", len(path) < 1)
            if(len(path) < 2):
                break
            pred = path[-2]
            #print("New pred: ", pred)
        #add prestige to the root
        #print("Adding ", prestige_transit, "to root ", node)
        users[node].gained_mal += maliciousness_transit

    for user in users:
        users[user].maliciousness += users[user].gained_mal
        users[user].gained_mal = 0

    return;


def generateUsersPrestigeMaliciousness(size, max_prestige, max_maliciousness):
    #Generate users and allocates money and prestige
    users = {}
    for i in range(0, int(size)):
        #don't put 0 prestige on the nodes, it's used for error checking

        #users[i] = User(i, 0, uniform(10, max_prestige))
        prest=uniform(10, max_prestige)
        mal=uniform(0, max_maliciousness)
        users[i] = User(i, 0, prest, mal)


        #(self, id, coins = 0, prestige = 0, d = 0.1, f = 10, frequency = 0, work_prob = 0):
        print(users[i])

    return users

def generateUsersPrestigeFromGraph(max_prestige, G):
    #Generate users and allocates money and prestige
    users = {}
    for node in G.nodes:
        #don't put 0 prestige on the nodes, it's used for error checking
        users[node] = User(node, 0, uniform(10, max_prestige))
        print(users[node])

    return users

users_coins = {}
users_prestige = {}
users_stats_counter = -1



#######################################
# Statistics section                  #
# Should be moves to a different file #
#######################################

# tracks user amount of coins and prestige
def generateUserStats(users, inc = 1):
    global users_stats_counter
    users_stats_counter += inc
    if(users_stats_counter == 0): # initialise list in the beginning
        for user in users:
            users_coins[user] = list()
            users_prestige[user] = list()
    # add new amount of coins and presitge to list
    for user in users:
        print("Counter: ", users_stats_counter)
        users_coins[user].append((users_stats_counter, users[user].coins))
        users_prestige[user].append((users_stats_counter, users[user].prestige))




def printUsersStats(users):
    global users_stats_counter
    for user in users:
        keys = list([i[0] for i in users_prestige[user]])
        # i[0] is the first element of users_prestige[user] list
        # i.e. the counter number
        vals = list([i[1] for i in users_prestige[user]])
        # i[1] is the second element of users_prestige[user] list
        # i.e. the amount of prestige
        user_label = "C=" + str(users[user].coins) + ", d=" + str(users[user].d) # C = coins, d = decay parameter
        styles = ['dashed', 'dashdot', 'dashdot', 'dotted', 'solid']
        plt.plot(keys, vals, label=user_label, linewidth=2.2, linestyle=styles[user])
        plt.plot((0, users_stats_counter), (users[user].staticValue(), users[user].staticValue()), 'k-', linestyle='dashed')

    plt.xlabel('Time[blocks]')
    plt.ylabel('Prestige')
    #plt.title("Prestige evolution over time")
    plt.legend()
    plt.show()

    for user in users:
        keys = list([i[0] for i in users_coins[user]])
        # same as above but for coins
        vals = list([i[1] for i in users_coins[user]])
        # same as above but for coins
        user_label = "user " + str(user)
        plt.plot(keys, vals, label=user_label)

    plt.xlabel('Time[blocks]')
    plt.ylabel('Coins')
    plt.title("Coins evolution over time")
    plt.legend()
    #plt.show()

stats = {}
stats['node'] = list()
stats['distance'] = list()
stats['transfers'] = list()
stats['prestige'] = list()
stats['gain'] = list()
stats['type'] = list()
stats['iter'] = list()

def generateStats(orig_users, users, G, type, iter):

    paths = nx.single_source_shortest_path_length(G, 0)
    for node in G.nodes:
        stats['node'].append(node)
        stats['distance'].append(paths[node])
        stats['transfers'].append(G.out_degree(node))
        stats['prestige'].append(orig_users[node].prestige)
        stats['gain'].append(users[node].prestige  - orig_users[node].prestige)
        stats['type'].append(type)
        stats['iter'].append(iter)




def printStats():
    df = pd.DataFrame(stats)
    print(stats)

    fig, ax = plt.subplots()
    for key, group in df.groupby('type'):
        avg = group.groupby('transfers')['gain'].mean()
        std = group.groupby('transfers')['gain'].std()
        ax.set_xlabel("ax label")
        bx = avg.plot(x='transfers', y='gain', yerr=std, ax=ax, legend=True);
        bx.set_xlabel("Number of performed services")
        bx.set_ylabel("Prestige gained")

    plt.legend(labels=['Progressive', 'Simple'])
    #plt.show()

    fig, ax = plt.subplots()
    for key, group in df.groupby('type'):
        avg = group.groupby('distance')['gain'].mean()
        std = group.groupby('distance')['gain'].std()
        bx =avg.plot(x='distance', y='gain', yerr=std, ax=ax, legend=True);
        bx.set_xlabel("Distance from root")
        bx.set_ylabel("Prestige gained")


    plt.legend(labels=['Progressive', 'Simple'])

    fig, ax = plt.subplots()
    for key, group in df.groupby('type'):
        bx = group.plot('prestige', 'gain', style='o', ax=ax, legend=True);
        bx.set_xlabel("Base prestige")
        bx.set_ylabel("Prestige gained")

    plt.legend(labels=['Progressive', 'Simple'])
    plt.show()



#read a topology from a one simulator traces file
def readFile(filename):
    print("Reading file " + filename)
    trees = {}
    roots = {}
    delimiter = " "
    f = open(filename,"r")
    for line in f:
        if line.startswith("#"):
            continue
        source = line.split(delimiter)[5]
        destination = line.split(delimiter)[6]
        tree = line.split(delimiter)[1]

        #print(line)
        #print("Got source:", source, " destination: ", destination, " tree: ", tree)

        #Create a new graph if necessary
        if(tree not in trees):
            trees[tree] = nx.DiGraph()

        #Add the edge to the graph
        trees[tree].add_edge(source, destination)

        #Record the origins for each tree
        #if(tree not in roots):
        #    roots[tree] = set()
        #roots[tree].add(origin)
    f.close()

    #find origin nodes (no parent)
    for tree in trees:
        for node in trees[tree]:
#            print("Checking node ", node, " tree ", tree)
            #for pred in trees[tree].predecessors(node):
            #    print(pred)
            if len(list(trees[tree].predecessors(node))) == 0:
                if(tree not in roots):
                    roots[tree] = set()
                roots[tree].add(node)
            if len(list(trees[tree].predecessors(node))) > 1:
                print("Incorrect tree with one more predescessor")
                quit()



    print("Found ", len(trees), " trees:")

    for tree in trees:
        DG = trees[tree]
        #Add the publisher as root to all the trees
        for origin in roots[tree]:
            DG.add_edge(0, origin)
        print("Tree ", tree, " with ", len(DG.nodes), " nodes")

        #print("Nodes: ")
        #counter=0
        #for node in DG.nodes:
        #    counter += 1
        #    print(counter," ", node)

        #print("Edges: ")
        #counter=0
        #for edge in DG.edges:
        #    counter += 1
        #    print(counter," ", edge)

        #pos = nx.spring_layout(DG)  # positions for all nodes
        #nx.draw_networkx_nodes(DG, pos, node_size=700)
        #nx.draw_networkx_edges(DG, pos, width=6)
        #nx.draw_networkx_labels(DG, pos, font_size=20, font_family='sans-serif')
        #plt.axis('off')
        #plt.show()

    return trees

#Generate a random graph
def generateRandomTree(nodes):
    G = nx.random_tree(int(nodes))
    #convert the graph to a tree (unidirectional edges)
    return nx.bfs_tree(G,0).to_directed() # already directed, returns deep copy

#######################################
# Arnold added section                #
#######################################

def generateUsersCoinPrestige(size, max_coin, max_prestige):
    #Generate users and allocates money and prestige
    users = {}
    for i in range(0, int(size)):
        #don't put 0 prestige on the nodes, it's used for error checking
        users[i] = User(i, uniform(10, max_coin), uniform(10, max_prestige))
        #(self, id, coins = 0, prestige = 0, d = 0.1, f = 10, frequency = 0, work_prob = 0):
        print(users[i])

    return users
