import sys
import copy
from random import expovariate, gauss, uniform
from user import *
from simulator import *

#import pstats
#import cProfile



def printHelp():
    print("Usage: python3 bbc.py <number_of_users> <f> <b> ");
    quit()

def running(n_nodes, f, b):

    users_orig = generateUsersPrestige(n_nodes, 1000)
    #Copy the original users for simple and progressive mining
    users_prog = copy.deepcopy(users_orig)
    DG = generateRandomTree(n_nodes)

    #perform simulation for progressive mining
    calculatePrestige(users_prog, DG, f=f, b=b)
    generateStats(users_orig, users_prog, DG, 'prog', 1)



if(len(sys.argv) == 4):
    print("Generating")
else:
    printHelp()
    quit()

nUsers = int(sys.argv[1])
f = float(sys.argv[2])
b = float(sys.argv[3])
#cProfile.run('running()', "profiler_stats")

running(nUsers, f, b)
#p = pstats.Stats('profiler_stats')
#p.sort_stats('time').print_stats(10)

df = pd.DataFrame(stats)

df.to_csv("bbc_stats_"+str(nUsers)+ "_" +str(f) + "_" + str(b) + ".csv")

#print(df)

#df.hist('gain', cumulative=True, label='Empirical', histtype='step', bins=100, linestyle='--')

#plt.show()

#print statistics
#printStats()
