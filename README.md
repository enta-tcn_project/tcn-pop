## Simulator
The repository contains a modified version of the Proof-of-Prestige (PoP) simulator.
This version was developed in the context of the NGI_TRUST 'Trusted Content Networking (TCN)' project. It is designed to allow experimentation with reputation-based trust mechanisms on top of PoP.

### Code
The original code and installation instructions can be found in https://gitlab.com/mharnen/pop

#### Version notes
Version 0.2 (dev)
New maliciousness calculation method added in both Simple and Progressive mining. Maliciousness values now range between 0 and 1 and their calculation is different from the one used for prestige.
Users now choose whether to accept a file from a contributor, based on its maliciousness.
