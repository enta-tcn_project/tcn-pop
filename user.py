import random

debugging = 0 # [AS] Added a switch for debugging

class User:

    def __init__(self, id, coins = 0, prestige = 0, maliciousness = 0, d = 0.1, dm = 0.01, f = 10, frequency = 0, work_prob = 0, mal_prob = 100):
        self._id = id
        self._coins = coins
        self._prestige = prestige
        self._maliciousness = maliciousness
        self.gained = 0
        self.gained_mal = 0
        self._d = d
        self._dm = dm
        self._sum = 0
        self._sum_mal = 0
        self._f = f
        self._counter = 0
        self._frequency = frequency
        self._work_prob = work_prob
        self._offset = 0 #[AS] Indicates each user's number of outgoing transfers
        self._mal_prob = mal_prob # [AS] Added a new parameter: probability of a user sending malicious content


    @property
    def prestige(self):
        return self._prestige
    @prestige.setter
    def prestige(self, value):
        self._prestige = value

# [AS] new #
    @property
    def maliciousness(self):
        return self._maliciousness
    @maliciousness.setter
    def maliciousness(self, value):
        self._maliciousness = value
# //[AS] #

    @property
    def gained(self):
        return self._gained
    @gained.setter
    def gained(self, value):
        self._gained = value

# [AS]
    @property
    def gained_mal(self):
        return self._gained_mal
    @gained_mal.setter
    def gained_mal(self, value):
        self._gained_mal = value

    @property
    def frequency(self):
        return self._frequency
    @property
    def id(self):
        return self._id
    @property
    def sum(self):
        return self._sum

# [AS]
    @property
    def sum_mal(self):
        return self._sum_mal
# // [AS] 

    @property
    def f(self):
        return self._f

    @property
    def coins(self):
        return self._coins

    @coins.setter
    def coins(self, value):
        self._coins = value

    def isWorking(self):
        result =  (random.randint(0,100) < self._work_prob)
        print("\nUser ", self._id)
        print ("Original working offset is: ", self._offset)
        if((result == True) & (self._offset == 0)):
            print ("result==True and working offset is 0, isWorking() returns True")
            return True
        elif((result == True) & (self._offset > 0)) :
            self._offset -= 1
            print ("result==True but working offset is > 0, isWorking() modifies offset and returns False")
            print ("Modified working offset is: ", self._offset)
            return False
        else:
            print ("result==False, returns False")
            return False

# [AS] Added a function for malicious users
    # def isMalicious(self):
    #     result =  (random.randint(0,100) < self._mal_prob)
    #     if((result == True) & (self._offset == 0)):
    #         return True
    #     elif((result == True) & (self._offset > 0)) :
    #         self._offset -= 1
    #         return False
    #     else:
    #         return False            

    def isMalicious(self):
        result =  (random.randint(0,100) < self._mal_prob)
        if((result == True)):
            return True
        else:
            return False             

    def addOffset(self, inc):
        self._offset += inc


    @property
    def d(self):
        return self._d

    @property
    def dm(self):
        return self._dm

    @d.setter
    def d(self, value):
        self._d = value


    # Prestige gained when done work
    # Prestige decay
    def update(self):


        if(self._frequency > 0):
            if ((self._counter % self._frequency) == 0):
                print("Inside update():")
                print("f = ", self._f)
                print("prestige = ", self._prestige)
                print("maliciousness= ", self._maliciousness)
                print("first update")
                self._prestige += self._f
                self._maliciousness += self._f # [AS] currently assuming same fine as prestige
                print("prestige = ", self._prestige)
                print("maliciousness= ", self._maliciousness) 
            #print("Adding ", self._f, " prestige.")

        if debugging: print("Prestige before decay: ", self._prestige)
        if debugging: print("Maliciousness before decay: ", self._maliciousness)
        self._counter += 1
        if debugging: print("_sum before addition: ", self._sum)
        if debugging: print("_prestige before addition: ", self._prestige)
        self._sum += self._prestige
        if debugging: print("_sum after addition: ", self._sum)
        if debugging: print("(prestige before calculation): ", self._prestige)
        self._prestige += self._coins - (self._prestige * self._d)
        #self._prestige += 0 - (self._prestige * self._d) #[AS] Experimenting!
        if debugging: print("(prestige after calculation): ", self._prestige)
        #print("Prestige after decay: ", self._prestige)

# [AS] Repeat for maliciousness
        if debugging: print("_sum_mal before addition: ", self._sum_mal)
        if debugging: print("_maliciousness before addition: ", self._maliciousness)
        self._sum_mal += self._maliciousness
        if debugging: print("_sum_mal after addition: ", self._sum_mal)
        #print("(before maliciousness calculation) d= ", self._d)
        if debugging: print("(maliciousness before calculation): ", self._maliciousness)
        self._maliciousness += self._coins - (self._maliciousness * self._dm)
        #self._maliciousness += 0 - (self._maliciousness * self._dm) #[AS] Experimenting!
        if debugging: print("(maliciousness after calculation): ", self._maliciousness)


        # print("sum = ", self._sum)
        # print("sum_mal = ", self._sum_mal)
        # print("Prestige after decay: ", self._prestige)
        # print("Maliciousness after decay: ", self._maliciousness)
        # print("")


    def staticValue(self):
        print("static value:", self._coins / self._d)
        return (self._coins / self._d)

    def __str__(self):
        return "User: " + str(self.id) + ", coins: " + str(self._coins) + ", prestige: " + str(self.prestige) + ", maliciousness: " + str(self.maliciousness) + ", gained: " + str(self.gained) + ", sum: " + str(self._sum) + ", sum_mal: " + str(self._sum_mal)
