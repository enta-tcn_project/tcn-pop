#!/bin/bash

users=10

for f in 10 20 50 100
do
    for b in 0.1 0.5 1 1.5 2
    do
        echo users=$users f=$f b=$b
        python3 ./bbc.py $users $f $b &
    done
done
