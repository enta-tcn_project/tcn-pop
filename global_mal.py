import sys
from random import expovariate, gauss, uniform
from user import *
from simulator import *
import matplotlib
import random
import copy


font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 12}

matplotlib.rc('font', **font)

users_simple_coins = {}
users_simple_prestige = {}

users_prog_coins = {}
users_prog_prestige = {}
users_stats_counter = -1

stats = {}
stats['block'] = list()
stats['user'] = list()
stats['prestige'] = list()
stats['maliciousness'] = list()
stats['mining'] = list()
stats['W'] = list()
stats['coins'] = list()


def generateGlobalStats(users_simple, users_prog, inc = 1):
    global users_stats_counter
    global local_counter
    users_stats_counter += inc
    print("Increasing counter to ", users_stats_counter)
    #counter to distinguish between simple and progressive mining

    if(users_stats_counter == 0):
        for user in users_simple:
            users_simple_coins[user] = list()
            users_simple_prestige[user] = list()
            users_simple_maliciousness[user] = list()


        for user in users_prog:
            users_prog_coins[user] = list()
            users_prog_prestige[user] = list()
            users_prog_maliciousness[user] = list() #[AS]


    for user in users_simple:
        users_simple_coins[user].append((users_stats_counter, users_simple[user].coins))
        users_simple_prestige[user].append((users_stats_counter, users_simple[user].prestige))
        users_simple_maliciousness[user].append((users_stats_counter, users_simple[user].maliciousness)) #[AS]

    for user in users_prog:
        users_prog_coins[user].append((users_stats_counter, users_prog[user].coins))
        users_prog_prestige[user].append((users_stats_counter, users_prog[user].prestige))
        users_prog_maliciousness[user].append((users_stats_counter, users_prog[user].maliciousness)) #[AS]

def printStats():
    df = pd.DataFrame(stats) # [AS] // pd-> pandas ('import pandas as pd')
    print("Printing DataFrame (imported from stats):")
    print(df)

   # [AS] Plot Prestige -> Temporarily disabled

    for key, group in df.groupby('mining'):
        print("key", key)
        print("group")
        print(group)
        fig, ax = plt.subplots() # [AS] plt-> matplotlib.pyplot ('import matplotlib.pyplot as plt')
        #print("fig, ax :", fig, ax)
        ax.set_ylabel("Prestige")
        counter = 0
        for subkey, subgroup in group.groupby('W'):
            print("subkey", subkey)
            print("subgroup")
            print(subgroup)
            for subsubkey, subsubgroup in subgroup.groupby('user'):
                #print("subgroup:", subgroup)
                print("subsubkey:", subsubkey)
                print("subsubgroup")
                print(subsubgroup)
                styles = ['solid', 'dashed', 'dashdot', 'dotted']
                if(key == 'simple'):
                    user_label = "C=" + str(users_simple[subsubkey].coins) + ", W=" + str(subkey)
                else:
                    user_label = "C=" + str(users_simple[subsubkey].coins) + ", W=" + str(subkey)
                bx = subsubgroup.plot('block', 'prestige', ax=ax, label=user_label, legend=True, linewidth=2.5, linestyle=styles[counter]);
                bx.set_xlabel("Time[blocks]")
                plt.plot((0, users_stats_counter), (users_simple[subsubkey].staticValue(), users_simple[subsubkey].staticValue()), 'k-', linestyle='dashed')
                counter+=1
        plt.show()

   #      [AS] Plot maliciousness

    for key, group in df.groupby('mining'):
        print("key", key)
        print("group")
        print(group)
        fig, ax = plt.subplots() # [AS] plt-> matplotlib.pyplot (import matplotlib.pyplot as plt)
        #print("fig, ax :", fig, ax)
        ax.set_ylabel("Maliciousness")
        counter = 0
        for subkey, subgroup in group.groupby('W'):
            print("subkey", subkey)
            print("subgroup")
            print(subgroup)
            for subsubkey, subsubgroup in subgroup.groupby('user'):
                #print("subgroup:", subgroup)
                print("subsubkey:", subsubkey)
                print("subsubgroup")
                print(subsubgroup)
                styles = ['solid', 'dashed', 'dashdot', 'dotted']
                if(key == 'simple'):
                    user_label = "C=" + str(users_simple[subsubkey].coins) + ", W=" + str(subkey)
                else:
                    user_label = "C=" + str(users_simple[subsubkey].coins) + ", W=" + str(subkey)
                    #user_label = "C=" + str(users_progressive[subsubkey].coins) + ", W=" + str(subkey) # [AS] Typo? Changed users_simple[] to users_prog[]
                bx = subsubgroup.plot('block', 'maliciousness', ax=ax, label=user_label, legend=True, linewidth=2.5, linestyle=styles[counter]);
                bx.set_xlabel("Time[blocks]")
                plt.plot((0, users_stats_counter), (users_simple[subsubkey].staticValue(), users_simple[subsubkey].staticValue()), 'k-', linestyle='dashed')
                counter+=1
        plt.show()



def collectStats(users_simple, users_prog, block):
    for user in users_simple:
        stats['block'].append(block)
        stats['user'].append(user)
        stats['coins'].append(users_simple[user].coins)
        stats['prestige'].append(users_simple[user].prestige)
        stats['maliciousness'].append(users_simple[user].maliciousness)
        stats['W'].append(W)
        stats['mining'].append('simple')

    for user in users_prog:
        stats['block'].append(block)
        stats['user'].append(user)
        stats['coins'].append(users_prog[user].coins)
        stats['prestige'].append(users_prog[user].prestige)
        stats['maliciousness'].append(users_prog[user].maliciousness)
        stats['W'].append(W)
        stats['mining'].append('prog')



def printGlobalStats(users_simple, users_prog):
    global users_stats_counter
    local_counter = 0
    for user in users_simple:
        keys = list([i[0] for i in users_simple_prestige[user]])
        vals = list([i[1] for i in users_simple_prestige[user]])
        user_label = "Simple, C=" + str(users_simple[user].coins)
        plt.plot(keys, vals, label=user_label, linewidth=2.2, linestyle='solid')
        plt.plot((0, users_stats_counter), (users_simple[user].staticValue(), users_simple[user].staticValue()), 'k-', linestyle='dashed')

# [AS]
    # for user in users_simple:
    #     keys = list([i[0] for i in users_simple_maliciousness[user]])
    #     vals = list([i[1] for i in users_simple_maliciousness[user]])
    #     user_label = "Simple, C=" + str(users_simple[user].coins)
    #     plt.plot(keys, vals, label=user_label, linewidth=2.2, linestyle='solid')
    #     plt.plot((0, users_stats_counter), (users_simple[user].staticValue(), users_simple[user].staticValue()), 'k-', linestyle='dashed')


    for user in users_prog:
        keys = list([i[0] for i in users_prog_prestige[user]])
        vals = list([i[1] for i in users_prog_prestige[user]])
        user_label = "Progressive, C=" + str(users_prog[user].coins)
        plt.plot(keys, vals, label=user_label, linewidth=2.2, linestyle='dashed')
        #plt.plot((0, users_stats_counter), (users[user].staticValue(), users[user].staticValue()), 'k-', linestyle='dashed')


    print("users_simple_coins: ", users_simple_coins)
    print("users_prog_coins: ", users_prog_coins)
    print("users_simple_prestige: ", users_simple_prestige)
    print("users_prog_prestige: ", users_prog_prestige)
    print("users_simple_maliciousness: ", users_simple_maliciousness)
    print("users_prog_maliciousness: ", users_prog_maliciousness)


    plt.xlabel('Time[blocks]')
    plt.ylabel('Prestige')
    #plt.title("Prestige evolution over time")
    plt.legend()
    plt.show()






def printHelp():
    print("usage ./function <number_of_iterations>")
    quit()


if(len(sys.argv) == 2):
    nIterations = int(sys.argv[1])
    print("Running ",  nIterations, " iterations")
else:
    printHelp()
    quit()


# ORIGINAL PARAMETERS

#tree_size = 100
#Ws = [5, 20]
#W = 0
#f = 200
#d = 0.03
#users_stats_counter = nIterations


# [AS] ALTERED PARAMETERS  #

tree_size = 100
Ws = [5, 30]
W = 0
f = 200
d = 0.03 # Prestige decay factor
dm = 0.002 # Maliciousness decay factor
users_stats_counter = nIterations


# [AS] END OF ALTERED PARAMETERS # 

for w in Ws:
    W = w
    initial_prestige = 1000

# [AS] Note: users_simple is a dictionary; 0 is the key, User(...) is the value
# [AS]  def __init__(self, id, coins = 0, prestige = 0, maliciousness = 0, d = 0.1, dm=0.01, f = 10, frequency = 0, work_prob = 0, mal_prob = 100):

    users_simple = {}
    users_simple[0] = User(0, 200, 0, d=d, dm=dm, work_prob = W, mal_prob=100)
    users_simple[1] = User(1, 200, 0, d=d, dm=dm, work_prob = W, mal_prob=0)

    users_progressive = {}
    users_progressive[0] = User(2, 200, 0, d=d, dm=dm, work_prob = W, mal_prob=100)
    users_progressive[1] = User(3, 200, 0, d=d, dm=dm, work_prob = W, mal_prob=0)

    # users_simple = {}
    # users_simple[0] = User(0, 50, 0, d=d, dm=dm, work_prob = W, mal_prob=100)
    # users_simple[1] = User(1, 200, 0, d=d, dm=dm, work_prob = W, mal_prob=100)
    # users_simple[2] = User(2, 50, 0, d=d, dm=dm, work_prob = W, mal_prob=50)
    # users_simple[3] = User(3, 200, 0, d=d, dm=dm, work_prob = W, mal_prob=50)

    # users_progressive = {}
    # users_progressive[0] = User(4, 50, 0, d=d, dm=dm, work_prob = W, mal_prob=100)
    # users_progressive[1] = User(5, 200, 0, d=d, dm=dm, work_prob = W, mal_prob=100)
    # users_progressive[2] = User(4, 50, 0, d=d, dm=dm, work_prob = W, mal_prob=50)
    # users_progressive[3] = User(5, 200, 0, d=d, dm=dm, work_prob = W, mal_prob=50)

    # for i in range(0, nIterations):
    #     working_set = set()

    #     for user in users_simple.values():
    #         if(user.isWorking() == True):
    #            working_set.add(user)

# deprecated [AS] Added a set for malicious users #
        # malicious_set = set()
        # for working_user in working_set:
        #     if(working_user.isMalicious() == True):
        #         malicious_set.add(working_user)
# deprecated [AS] End

# [AS] Added a set for malicious users; A subset of the working users is going to be malicious #
    for i in range(0, nIterations):
        working_set = set()
        malicious_set = set()
        print("Iteration # ", i)
        for user in users_simple.values():
            if(user.isWorking() == True):
                working_set.add(user)
                print("User ", user.id, " is working!")
                if (user.isMalicious()==True):
                    malicious_set.add(user)
                    print("User ", user.id, " is malicious!")
# [AS] End


        if (len(working_set) > 0):
            users_tree_orig_simple = generateUsersPrestige(tree_size, initial_prestige)
            users_tree_orig_prog = copy.deepcopy(users_tree_orig_simple)
            users_tree_simple = copy.deepcopy(users_tree_orig_simple)
            users_tree_prog = copy.deepcopy(users_tree_orig_prog)
            G = generateRandomTree(tree_size)

            bindings = {}
            for user in working_set:
                #choose a user to replace

                bindings[user.id] = random.randint(0, tree_size - 1)
                print("user ", user.id, " replaces #", bindings[user.id])

                #increase offset by number of transfers
                users_simple[user.id].addOffset(G.out_degree(bindings[user.id]))
                users_progressive[user.id].addOffset(G.out_degree(bindings[user.id]))

                #replace the corresponding user in simple minig tree
                users_tree_simple[bindings[user.id]].prestige = users_simple[user.id].prestige
                users_tree_orig_simple[bindings[user.id]].prestige = users_simple[user.id].prestige

                users_tree_simple[bindings[user.id]].maliciousness = users_simple[user.id].maliciousness
                users_tree_orig_simple[bindings[user.id]].maliciousness = users_simple[user.id].maliciousness

                users_tree_simple[bindings[user.id]].coins = users_simple[user.id].coins

                #replace the corresponding user in progressive minig tree
                users_tree_prog[bindings[user.id]].prestige = users_progressive[user.id].prestige
                users_tree_orig_prog[bindings[user.id]].prestige = users_progressive[user.id].prestige

                users_tree_prog[bindings[user.id]].maliciousness = users_progressive[user.id].maliciousness
                users_tree_orig_prog[bindings[user.id]].maliciousness = users_progressive[user.id].maliciousness

                users_tree_prog[bindings[user.id]].coins = users_progressive[user.id].coins


            #perform prestige transfer simulations for simple and progressive mining
            calculatePrestigeSimple(users_tree_simple, G, f=f)
            calculatePrestige(users_tree_prog, G, f=f)

            calculateMaliciousnessSimple(users_tree_simple, G, f=f)
            calculateMaliciousness(users_tree_prog, G, f=f)

            for user in working_set:
                print("Original prestige (users_tree_orig_simple): ", users_tree_orig_simple[bindings[user.id]].prestige)
                print("Prestige after calculations (users_tree_simple): ", users_tree_simple[bindings[user.id]].prestige)
                simple_prestige_gain = users_tree_simple[bindings[user.id]].prestige  - users_tree_orig_simple[bindings[user.id]].prestige
                users_simple[user.id].prestige += simple_prestige_gain
                print("Final prestige assigned to user (users_simple): ", users_simple[user.id].prestige)

                print("\nOriginal prog prestige (users_tree_orig_prog): ", users_tree_orig_prog[bindings[user.id]].prestige)
                print("Prog prestige after calculations (users_tree_prog): ", users_tree_prog[bindings[user.id]].prestige)
                prog_prestige_gain = users_tree_prog[bindings[user.id]].prestige  - users_tree_orig_prog[bindings[user.id]].prestige
                users_progressive[user.id].prestige += prog_prestige_gain
                print("Final prog prestige assigned to user (users_prog): ", users_progressive[user.id].prestige)

# [AS] Added actions in case user is malicious 
                if user in malicious_set:
                    print("\nOriginal maliciousness (before SimpleMining calculation)", users_tree_orig_simple[bindings[user.id]].maliciousness)
                    print("Maliciousness after calculations (users_tree_simple): ", users_tree_simple[bindings[user.id]].maliciousness)
                    simple_maliciousness_gain = users_tree_simple[bindings[user.id]].maliciousness  - users_tree_orig_simple[bindings[user.id]].maliciousness
                    users_simple[user.id].maliciousness += simple_maliciousness_gain
                    print("Maliciousness gain: ", simple_maliciousness_gain)
                    print("Final maliciousness assigned to user (users_simple): ", users_simple[user.id].maliciousness)

                    print("\nOriginal prog maliciousness (users_tree_orig_prog): ", users_tree_orig_prog[bindings[user.id]].maliciousness)
                    print("Prog maliciousness after calculations (users_tree_prog): ", users_tree_prog[bindings[user.id]].maliciousness)
                    prog_maliciousness_gain = users_tree_prog[bindings[user.id]].maliciousness  - users_tree_orig_prog[bindings[user.id]].maliciousness
                    users_progressive[user.id].maliciousness += prog_maliciousness_gain
                    print("Prog maliciousness gain: ", prog_maliciousness_gain)
                    print("Final prog maliciousness assigned to user (users_prog): ", users_progressive[user.id].maliciousness)
                    # if debugging: print("\nMaliciousness before ProgMining calculation", users_tree_prog[bindings[user.id]].maliciousness)
                    # prog_maliciousness_gain = users_tree_prog[bindings[user.id]].maliciousness  - users_tree_orig_prog[bindings[user.id]].maliciousness
                    # users_progressive[user.id].maliciousness += prog_maliciousness_gain
                    # if debugging: print("Maliciousness ProgMining gain", prog_maliciousness_gain)
                    # if debugging: print("Maliciousness after ProgMining calculation", users_progressive[user.id].maliciousness)
                

        for user in users_simple.values():
            if debugging: print("Before SimpleMining update:")
            if debugging: print(user)
            user.update()
            if debugging: print("After SimpleMining update:")
            if debugging: print(user)
            if debugging: print("")

        for user in users_progressive.values():
            if debugging: print("Before ProgMining update:")
            if debugging: print(user)
            user.update()
            if debugging: print("After ProgMining update:")
            if debugging: print(user)
            if debugging: print("")

        #generateGlobalStats(users_simple, users_progressive)
        collectStats(users_simple, users_progressive, i)

print("stats", stats)
printStats()
#printGlobalStats(users_simple, users_progressive)

quit()

stats = {}
stats['node'] = list()
stats['distance'] = list()
stats['transfers'] = list()
stats['prestige'] = list()
stats['maliciousness'] = list()
stats['gain'] = list()
stats['type'] = list()
stats['iter'] = list()
